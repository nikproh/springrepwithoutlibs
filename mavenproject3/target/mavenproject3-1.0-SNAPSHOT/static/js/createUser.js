Ext.onReady(function(){
    var simple = new Ext.FormPanel({
        labelWidth: 150,
        frame:true,
        title: 'Create User Form',
        bodyStyle:'padding:5px 5px 0',
        width: 350,
        defaultType: 'textfield',

        items: [{
                fieldLabel: 'Id',
                name: 'id'
            },{
                fieldLabel: 'Name',
                name: 'name'
            }
        ],

        buttons: [{
            text: 'Add',
            handler: function() {
                var form = simple.getForm(); 
                form.url = 'app/createUserResult'; 
                form.standardSubmit = true; 
                form.method = 'POST'; 
                form.submit();
                
            }
        }]
    });

    simple.render(document.body);   
});