Ext.onReady(function(){
    var simple = new Ext.FormPanel({
        labelWidth: 150,
        frame:true,
        title: 'Delete User Form',
        bodyStyle:'padding:5px 5px 0',
        width: 350,
        defaultType: 'textfield',

        items: [{
                fieldLabel: 'Id entry for delete',
                name: 'id'
            }
        ],

        buttons: [{
            text: 'Delete',
            handler: function() {
                var form = simple.getForm(); 
                form.url = 'app/deleteUserResult'; 
                form.standardSubmit = true; 
                form.method = 'POST'; 
                form.submit();
                
            }
        }]
    });

    simple.render(document.body);   
});