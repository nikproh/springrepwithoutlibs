package com.mycompany.mavenproject3.controllers;


import com.mycompany.mavenproject3.models.User;
import com.mycompany.mavenproject3.services.UserService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;


public class AppController extends MultiActionController {    
    UserService userService;

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public ModelAndView showAllUsers(HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        List<User> list = userService.getAllUsers();
        ModelAndView modelAndView = new ModelAndView("showAllUsers");
        modelAndView.addObject("list", list);
        return modelAndView;
    }
   
    public ModelAndView createUserResult(HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        String name = request.getParameter("name");
        String id = request.getParameter("id");
        User user = new User(name, id);
        userService.addUser(user);        
        List<User> list = userService.getAllUsers();        
        ModelAndView modelAndView = new ModelAndView("showAllUsers");
        modelAndView.addObject("list", list);        
        return modelAndView;
    }
    
    public ModelAndView deleteUserResult(HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        String id = request.getParameter("id");
        userService.deleteUser(id);        
        List<User> list = userService.getAllUsers();        
        ModelAndView modelAndView = new ModelAndView("showAllUsers");
        modelAndView.addObject("list", list);        
        return modelAndView;
    }
    
    public ModelAndView updateUserResult(HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        String name = request.getParameter("name");
        String id = request.getParameter("id");
        User user = new User(name, id);
        userService.updateUser(user);        
        List<User> list = userService.getAllUsers();        
        ModelAndView modelAndView = new ModelAndView("showAllUsers");
        modelAndView.addObject("list", list);        
        return modelAndView;
    }
}