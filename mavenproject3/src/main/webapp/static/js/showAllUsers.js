Ext.onReady(function(){
    var store = new Ext.data.ArrayStore({
        fields: [
           {name: 'name'},
           {name: 'id'}
        ]
    });

    store.loadData(myData);

    var grid = new Ext.grid.GridPanel({
        store: store,
        columns: [
            {
                header   : 'Id', 
                width    : 75, 
                sortable : true,  
                dataIndex: 'id'
            },
            {
                id       : 'name',
                header   : 'Name', 
                width    : 100, 
                sortable : true, 
                dataIndex: 'name'
            }            
        ],
        stripeRows: true,
        autoExpandColumn: 'name',
        height: 250,
        width: 300,
        title: 'User List',
        stateful: true,
        stateId: 'grid'
    });

    grid.render('grid');
});